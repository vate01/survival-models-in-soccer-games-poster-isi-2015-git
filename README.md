This repository has the poster I presented in the ISI 2015 held in Rio. You can find more information 
about that event in https://www.isi2015.org.

The title of the presentation was: «Survival models in soccer games».

The abstract of my contribution is the following:

«In these lines it is proposed an application of survival analysis to sports to the purpose of 
determine the probabilities and times associated with changes in the score of each game. Such 
applications have very few antecedents and have not been studied for individual teams. It is 
developed a Markov chain with finite state space and are modelled the transitions between them. The 
modelling is first carried out empirically and without considering covariates. Then is modelled the 
dependence of the changes of state considering the inclusion of covariables. It was imposed as a 
prerequisite for these covariates that were easily accessible from web pages. At the end of the 
study, is expected having quantified numerically how much is the influence of the sanctions of yellow 
cards and locality on the final result for the games played by the Venezuela team. Also it is 
expected to know which are the most critical minutes during matches according the state where team is 
transiting at some moment.»

